//
//  AppDelegate.h
//  Calculator
//
//  Created by Rohit Singh on 01/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
