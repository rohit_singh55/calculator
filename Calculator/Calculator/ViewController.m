//
//  ViewController.m
//  Calculator
//
//  Created by Rohit Singh on 01/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Number1:(id)sender{
    SelectNumber=SelectNumber*10+1;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];
    
}

-(IBAction)Number2:(id)sender{
    SelectNumber=SelectNumber*10+2;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];

}
-(IBAction)Number3:(id)sender{
    SelectNumber=SelectNumber*10+3;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];

}
-(IBAction)Number4:(id)sender{
    SelectNumber=SelectNumber*10+4;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];
}
-(IBAction)Number5:(id)sender{
    SelectNumber=SelectNumber*10+5;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];


}
-(IBAction)Number6:(id)sender{

    SelectNumber=SelectNumber*10+6;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];


}
-(IBAction)Number7:(id)sender{

    SelectNumber=SelectNumber*10+7;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];


}
-(IBAction)Number8:(id)sender{
    SelectNumber=SelectNumber*10+8;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];


}
-(IBAction)Number9:(id)sender{

    SelectNumber=SelectNumber*10+9;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];

}
-(IBAction)Number0:(id)sender{

    SelectNumber=SelectNumber*10+0;
    screen.text=[NSString stringWithFormat:@"%i",SelectNumber];

}
-(IBAction)Multiply:(id)sender{
    if(RunningTotal==0) {
        RunningTotal=SelectNumber;
    }
    else{
        switch (Method) {
            case 1:
                RunningTotal=RunningTotal*SelectNumber;
                break;
                
            case 2:
                RunningTotal=RunningTotal/SelectNumber;
                break;
            case 3:
                RunningTotal=RunningTotal-SelectNumber;
                break;
                
            case 4:
                RunningTotal=RunningTotal+SelectNumber;
                break;
                
                
        }
    }
    Method=1;
    SelectNumber=0;

}
-(IBAction)Divide:(id)sender{
    if(RunningTotal==0) {
        RunningTotal=SelectNumber;
    }
    else{
        switch (Method) {
            case 1:
                RunningTotal=RunningTotal*SelectNumber;
                break;
                
            case 2:
                RunningTotal=RunningTotal/SelectNumber;
                break;
            case 3:
                RunningTotal=RunningTotal-SelectNumber;
                break;
                
            case 4:
                RunningTotal=RunningTotal+SelectNumber;
                break;
                
                
        }
    }
    Method=2;
    SelectNumber=0;

}
-(IBAction)Subtract:(id)sender{
    if(RunningTotal==0) {
        RunningTotal=SelectNumber;
    }
    else{
        switch (Method) {
            case 1:
                RunningTotal=RunningTotal*SelectNumber;
                break;
                
            case 2:
                RunningTotal=RunningTotal/SelectNumber;
                break;
            case 3:
                RunningTotal=RunningTotal-SelectNumber;
                break;
                
            case 4:
                RunningTotal=RunningTotal+SelectNumber;
                break;
                
                
        }
    }
    Method=3;
    SelectNumber=0;

}
-(IBAction)ADD:(id)sender{
    if(RunningTotal==0) {
        RunningTotal=SelectNumber;
    }
    else{
        switch (Method) {
            case 1:
                RunningTotal=RunningTotal*SelectNumber;
                break;
                
            case 2:
                RunningTotal=RunningTotal/SelectNumber;
                break;
            case 3:
                RunningTotal=RunningTotal-SelectNumber;
                break;
                
            case 4:
                RunningTotal=RunningTotal+SelectNumber;
                break;
                
                
        }
    }
    Method=4;
    SelectNumber=0;

}
-(IBAction)Equals:(id)sender{

    if(RunningTotal==0) {
        RunningTotal=SelectNumber;
    }
    else{
        switch (Method) {
            case 1:
                RunningTotal=RunningTotal*SelectNumber;
                break;
                
            case 2:
                RunningTotal=RunningTotal/SelectNumber;
                break;
            case 3:
                RunningTotal=RunningTotal-SelectNumber;
                break;
                
            case 4:
                RunningTotal=RunningTotal+SelectNumber;
                break;
                
                
        }
    }
    Method=0;
    SelectNumber=0;
    screen.text=[NSString stringWithFormat:@"%1.2f",RunningTotal];

}
-(IBAction)AllClear:(id)sender{

    Method=0;
    SelectNumber=0;
    RunningTotal=0;
    screen.text=[NSString stringWithFormat:@"%i",0];

}
@end
